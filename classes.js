class Person {
    constructor (name, age) {
        this.name = name
        this.age = age 
    }
}

class Baby extends Person {
    constructor (name, age, favToy) {
        super (name, age)
        this.favToy = favToy
    }
    play () {
        console.log (`This boy with ${this.name} and ${this.age} plays wiz such kinda toy ${this.favToy}`);
    }
}

const bossBaby = new Baby ('Sam', 3, 'lama' );
console.log (bossBaby);