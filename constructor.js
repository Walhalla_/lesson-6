class Person {
    constructor (userName, age) {
    this.userName = userName 
    this.age = age 
    }
    describe() {
        console.log(`He is ${this.userName} n he's ${this.age}`);
    }
}

const Jack = new Person ('Sam', 19);
const Jill = new Person ('Jill', 20)
Jack.describe();
Jill.describe();
